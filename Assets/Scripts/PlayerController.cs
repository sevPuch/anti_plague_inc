﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    [Header("Numeric variables")]
    public float movementSpeed = 1f;
    float xMove = 0;
    float zMove = 0;
    Vector3 movement;

    CharacterController player;

    void Start () {
        player = GetComponent<CharacterController>();
        movement.y = -9.8f;
	}
	
	void Update () {
        if (Input.GetAxis("Horizontal") != 0)
        {
            float xInput = Input.GetAxis("Horizontal") * movementSpeed;
            movement.x = xInput * movementSpeed;
        }
        if (Input.GetAxis("Vertical") != 0)
        {
            float yInput = Input.GetAxis("Vertical") * movementSpeed;
            movement.z = yInput * movementSpeed;
        }
        //Turn small input values to zero, otherwise bug.
        if(Input.GetAxis("Horizontal") >= -0.1 && Input.GetAxis("Horizontal") <= 0.1)
        {
            movement.x = 0;
        }
        if (Input.GetAxis("Vertical") >= -0.1 && Input.GetAxis("Vertical") <= 0.1)
        {
            movement.z = 0;
        }
        

        Vector3.ClampMagnitude(movement, Input.GetAxis("Horizontal") * movementSpeed);
        player.Move(movement);
    }
}
